﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proyectil : MonoBehaviour {

    // recuperar una referencia a un rigidbody
    private Rigidbody rb;

	// Use this for initialization
	void Start () {

        // código para obtener referencia
        // PUEDE REGRESAR NULL!
        rb = GetComponent<Rigidbody>();
        Rigidbody[] rbs = GetComponents<Rigidbody>();
        // 3 vectores que son sus amiguitos
        // - up
        // - forward
        // - right

        rb.AddForce(transform.up * 10, ForceMode.Impulse);
        //Destroy(gameObject, 2);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
