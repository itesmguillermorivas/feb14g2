﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canion : MonoBehaviour {

    public GameObject original;
    public Transform referencia;

    private IEnumerator enumerador, disparo;
    private Coroutine corutina;

	// Use this for initialization
	void Start () {
        // Find
        // no abusen de el
        // original = GameObject.Find("Bala");

        enumerador = pseudoHilo();
        corutina = StartCoroutine(enumerador);
        StartCoroutine("corutina2");

        disparo = Disparar();
	}
	
	// Update is called once per frame
	void Update () {

        float h = Input.GetAxis("Horizontal");
        transform.Translate(h * 5 * Time.deltaTime, 0, 0);

        float v = Input.GetAxis("Vertical");
        transform.Rotate(30 * v * Time.deltaTime, 0, 0);

        if (Input.GetKeyDown(KeyCode.Space)) {
            StartCoroutine(disparo);
        }

        if (Input.GetKeyUp(KeyCode.Space)) {
            StopCoroutine(disparo);
        }

        if (Input.GetKeyUp(KeyCode.C)) {
            StopAllCoroutines();
        }

        if (Input.GetKeyUp(KeyCode.X)) {
            StopCoroutine("corutina2");
        }

        if (Input.GetKeyUp(KeyCode.Z)) {
            StopCoroutine(corutina);
        }
	}

    // corutinas
    // mecanismo para pseudo concurrencia
    IEnumerator pseudoHilo() {
        while (true) { 
            yield return new WaitForSeconds(0.5f);
            print("HOLA AMIGUITOS");
        }
    }

    IEnumerator corutina2() {
        while (true) {
            print("ADIOS.");
            yield return new WaitForSeconds(0.3f);
        }
    }

    IEnumerator Disparar() {
        while (true) {
            // disparar
            Instantiate(
                original,
                referencia.position,
                referencia.rotation
                );
            yield return new WaitForSeconds(0.2f);
        }
    }
}
