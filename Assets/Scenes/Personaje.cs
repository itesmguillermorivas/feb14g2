﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Personaje : MonoBehaviour {

    // atributos públicos 
    // - clases de unity
    // - primitivos
    public float speed = 3;
    public Text textito;

    // ciclo de vida

    // sucede al inicio de la creación del componente
    // una sola vez
    void Awake() {
        print("AWAKE");
    }
    
    // sucede después de awake
    // una sola vez también
    // start solo corre si el componente está habilitado
	// Use this for initialization
	void Start () {
        print("START");
        textito.text = "BUENOS DIAS AMIGUITOS :@";
    }
	
    // corre con el ciclo del juego
    // va a correr tantas veces pueda
    // 30 fps - aplicación en tiempo real
    // 60 fps 
	// Update is called once per frame
    // usar para:
    // - entrada de usuario
    // - desplazamiento
	void Update () {
        //print("UPDATE");

        // entrada en general 2 categorías
        // - polling: sondeo
        // - events / listeners

        // todo es polling aquí
        // 2 maneras de acceder a la entrada del usuario
        // 1 - directo a dispositivo
        // 2 - utilizando ejes

        if (Input.GetKeyDown(KeyCode.Space)) {
            // true si en frame anterior estaba libre
            // y en frame actual está presionado
            print("DOWN");
        }

        if (Input.GetKey(KeyCode.Space)) {
            // si estaba presionado en el frame anterior
            // y sigue presionado
            print("KEY");
        }

        if (Input.GetKeyUp(KeyCode.Space)) {
            // si estaba presionado en el frame anterior
            // y está liberado en el actual
            print("UP");
        }

        if (Input.GetMouseButtonDown(0)) {
            print(Input.mousePosition);
        }

        // 2 - por medio de ejes
        // ejes - abstracción de dispositivo de entrada
        // siempre regresan un valor flotante
        // rango: [-1, 1]
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        //print(h);

        // Transform - clase que define al componente
        // transform - objeto prepoblado con una referencia
        // al transform del mismo gameobject

        // velocidad basada en framerate
        // velocidad basada en tiempo
        transform.Translate(
            speed * h * Time.deltaTime,
            speed * v * Time.deltaTime, 
            0,
            Space.World
        );
    }

    // corre 1 vez al frame 
    // al acabar TODOS los updates
    // por cada componente por cada gameobject
    void LateUpdate() {
        //print("LATE UPDATE");
    }

    // fixed - fijo
    // update fijo: frecuencia "constante"
    void FixedUpdate() {
        //print("FIXED UPDATE");
    }

    // para detectar colisión con motor de física
    // - los involucrados tienen un collider
    // - el que se mueve tiene un rigidbody

    // detección de colisión a nivel código
    void OnCollisionEnter(Collision c) {
        print("ENTER: " + c.transform.name);
        print("UN PUNTO: " + c.contacts[0].point);
    }

    void OnCollisionStay(Collision c) {
        //print("STAY");
    }

    void OnCollisionExit(Collision c) {
        print("EXIT");
    }

    // triggers - 
    // colliders con los que puedo chocar PERO no me afectan
    // físicamente
    void OnTriggerEnter(Collider c) {
        print(c.transform.name);
    }
    void OnTriggerStay(Collider c) { }
    void OnTriggerExit(Collider c) { }
}
